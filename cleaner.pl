#!/usr/bin/perl

use strict;

sub die_with_help() {
	die `pod2text $0`;
}

die_with_help() unless @ARGV;

my ($file_bad_mail, $file_input) = @ARGV;

# Check existence of input files
unless (-e $file_bad_mail) {
	print STDERR "ERROR: File A ($file_bad_mail) does not exists";
}

unless (-e $file_input) {
	print STDERR "ERROR: File B ($file_input) does not exists";
}

if (not (-e $file_bad_mail and -e $file_input) ) {
	die_with_help();
}

my %bad_mail;

# Read file 1 to hash
open (my $fh,'<',$file_bad_mail);
while (<$fh>) {
	chomp;
	s/^\s//;
	s/\s$//;
	$bad_mail{$_} = 1 if $_;
}
close $fh;

# Read file 2
open (my $fh,'<',$file_input);
while (my $line = <$fh>) {
	chomp($line);
	$line =~ s/\r$//;
	my $email_not_exists = 1;
	MAIL:
	foreach my $mail (keys %bad_mail) {
#		print "\t$line\n";
#		print "\t$mail\n";
		if ( $line =~ qr([^-_\w\d]$mail) ) {
			$email_not_exists = 0;
			last MAIL;
		}
	}
	print "$line\n" if $email_not_exists;
}
close $fh;


__END__

=head1 NAME
  cleaner

=head1 SYNOPSYS
  ./cleaner.pl
       run without parametes will show this help
  ./cleaner.pl bad_mail.txt input.txt > output.txt
       run with parametes:
         bad_mail.txt - contains bad email adresses
         input.txt - is a file with alot of data per line.

         output.txt - All lines from file 2 with no one of the emails from file 1
=head1 DESCRIPTION
  There are 2 text files. 1 contains bad email adresses. 2 is a CSV file
  (may also be .txt) with alot of data per line. Example line:
  'name','1-2-1987','  [obscured]  '
  Please not the order of columns may very per file!
  All lines from file 2 must be deleted if one of the emails from file 1 are
  on that line. All clean lines must be written to file 3.
  
=head1 AUTHOR
  Stas Raskumandrin, stas@raskumandrin.ru, http://stas.raskumandrin.ru

=cut